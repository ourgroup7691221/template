variable "region" {
  type    = string
  default = "us-east-1"
}

variable "bucket_name" {
  type    = string
  default = "my-terraform-s3-bucket"
}

variable "acl" {
  type    = string
  default = "private"
}

variable "enable_versioning" {
  type    = bool
  default = true
}

variable "environment" {
  type    = string
  default = "dev"
}

