provider "aws" {
  region = var.region
}



module "s3_bucket" {
  source = "git::https://gitlab.com/test8811/list-s3-buckets.git//modules/s3?ref=main"
}